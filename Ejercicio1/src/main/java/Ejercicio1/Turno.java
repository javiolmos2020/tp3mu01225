package Ejercicio1;

import java.time.Duration;
import java.time.LocalDateTime;

public class Turno {

private Empleado[] empleadoTurno;
private LocalDateTime fechaHoraIngreso;
private LocalDateTime fechaHoraEgreso;

private final int TAMANIOEMPLEADO=10;


public Turno (LocalDateTime fechaHoraDeIngreso, LocalDateTime fechaHoraDeEgreso){
    this.fechaHoraIngreso = fechaHoraDeIngreso;
    this.fechaHoraEgreso = fechaHoraDeEgreso;
    
    empleadoTurno=new Empleado [TAMANIOEMPLEADO];
}

public void setFechaHoraDeIngreso(LocalDateTime fechaHoraDeIngreso) {
    this.fechaHoraIngreso = fechaHoraDeIngreso;
}

public LocalDateTime getFechaHoraDeIngreso() {
    return fechaHoraIngreso;
}

public void setFechaHoraDeEgreso(LocalDateTime fechaHoraDeEgreso) {
    this.fechaHoraEgreso = fechaHoraDeEgreso;
}

public LocalDateTime getFechaHoraDeEgreso() {
    return fechaHoraEgreso;
}

public Empleado[] getEmpleadoTurno() {
    return empleadoTurno;
}

public void setEmpleadoTurno(Empleado[] empleadoTurno) {
    this.empleadoTurno = empleadoTurno;
}

public long calcularHorasTrabajadas(LocalDateTime fechaHoraIngreso,LocalDateTime fechaHoraEgreso){

long resultado= Duration.between(fechaHoraEgreso,fechaHoraIngreso).toHours();

return resultado;
}

}