package Ejercicio1;

public class Empleado {

    private String nombre;
    private String apellido;
    private double costoHoraTrabajo;
    private Turno turnos [];
    private int cantidadTurno;
    
 //Método Constructor   
    public Empleado(String nombre, String apellido,double costoPorHoraDeTrabajo) {
    
    this.nombre = nombre;
    this.apellido = apellido;
    this.costoHoraTrabajo=costoPorHoraDeTrabajo;
    turnos=new Turno[10];
    
    }
    
//Métodos Setters y Getters    
    public void setNombre(String nombreEmpleado) {
        this.nombre = nombreEmpleado;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setApellido(String apellidoEmpleado) {
        this.apellido = apellidoEmpleado;
    }
    
    public String getApellido() {
        return apellido;
    }
    
    public void setCostoHoraDeTrabajo(double costoPorHoraDeTrabajo) {
        this.costoHoraTrabajo = costoPorHoraDeTrabajo;
    }
    
    public double getCostoHoraDeTrabajo() {
        return costoHoraTrabajo;
    }

    public void setTurnos(Turno[] turnosEmpleado){
        this.turnos=turnosEmpleado;
    }
  
    public Turno[] getTurnos(){
        return turnos;
    }

//Agregar turno
    public void agregarTurno(Turno turnoDeEmpleado){
       turnos[cantidadTurno++]=turnoDeEmpleado;
    }

//Cálculo ohoras de trabajo
    public long horasDeTrabajo(){

        long horasDeTrabajo=0;

        for(Turno turno : turnos){

        if(turno!=null){
        long horaTurno=turno.calcularHorasTrabajadas(turno.getFechaHoraDeEgreso(),turno.getFechaHoraDeIngreso());
        horasDeTrabajo=horasDeTrabajo+horaTurno;
       } 
       else{
            break;
        }
        
    }
    return horasDeTrabajo;

}

//Cálculo del sueldo
public double calculoDeSueldo(){
    double sueldo=0;
    sueldo=costoHoraTrabajo*horasDeTrabajo();
    return sueldo;   
    }

}