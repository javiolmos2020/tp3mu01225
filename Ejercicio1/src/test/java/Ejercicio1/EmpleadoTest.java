package Ejercicio1;
import static org.junit.Assert.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

import org.junit.Test;

public class EmpleadoTest {

    @Test public void TestGetApellido(){

Empleado empleado=new Empleado ("Javier", "Olmos", 120);

String apellido=empleado.getApellido();

assertEquals("Olmos",apellido);

    }


@Test

public void testCalcularHorasTrabajadas() {

    Empleado empleado=new Empleado ("Javier","Olmos", 20);

    DateTimeFormatter miformatFecha=DateTimeFormatter.ofPattern("dd/MM/yyyy  H:mm:ss");

Turno turnoEmpleado =new Turno(LocalDateTime.parse("27/09/2020  08:00:00",miformatFecha), LocalDateTime.parse("27/09/2020  12:00:00",miformatFecha)); 

Turno turnos []=new Turno [10];

turnos[0]=turnoEmpleado;

empleado.agregarTurno(turnoEmpleado);

long horasTrabajadas=empleado.horasDeTrabajo();

assertEquals(4, horasTrabajadas);

}


@Test public void TestCalcularSueldo(){


long horasDeTrabajo=4;

Empleado empleado=new Empleado ("Javier", "Olmos", 200);


double sueldo= empleado.getCostoHoraDeTrabajo()*horasDeTrabajo;

assertEquals(800,sueldo,0);

}
    
}



