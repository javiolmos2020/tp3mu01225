package Ejercicio2;


public class Cliente {
    private String nombreDeTitular;
    private String apellidoDeTitular;
 
    
    public void setNombreDelTitular(String nombreDelTitular) {
        this.nombreDeTitular = nombreDelTitular;
    }
    
    public String getNombreDeTitular() {
        return nombreDeTitular;
    }


    public void setApellidoDelTitular(String apellidoDelTitular) {
        this.apellidoDeTitular = apellidoDelTitular;
    }

    public String getApellidoDelTitular() {
        return apellidoDeTitular;
    }
    
}

    