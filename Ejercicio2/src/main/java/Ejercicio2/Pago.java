package Ejercicio2;

import java.time.LocalDateTime;



public class Pago extends Orden {

	private double precioTotal;

	public Pago(int numeroMesa, LocalDateTime horarioDeApertura, LocalDateTime horarioDeCierre, int cantidadDeComensales, String nombreDelMozo,
	int cantidadDePlatos, int cantidadDeBebidas, double precioDelPlato, double precioDeBebida, int metodoDePago) {
	super(numeroMesa, horarioDeApertura, horarioDeCierre, cantidadDeComensales, nombreDelMozo, cantidadDePlatos, cantidadDeBebidas, precioDelPlato,
		precioDeBebida, metodoDePago);

}


public void setPrecioTotal(double precioTotalAuxiliar) {
	this.precioTotal = precioTotalAuxiliar;
}

public double getPrecioTotal() {
	return precioTotal;
}


public double precioTotal(){

	double precioTotal=0;
	double subTotal=0;

    if(this.getMetodoPago()==1){

        precioTotal= (this.getPrecioBebida() * this.getCantidadBebidas())+(this.getPrecioPlato() * this.getCantidadPlatos());
	}

	if(this.getMetodoPago()==2){
       subTotal=(this.getPrecioBebida() * this.getCantidadBebidas())+(this.getPrecioPlato() * this.getCantidadPlatos());

       precioTotal=subTotal+(subTotal * 0.10);

	}

    return precioTotal;

    }

}
