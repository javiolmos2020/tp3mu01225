package Ejercicio2;
import java.time.LocalDateTime;

public class Orden {

protected int numeroMesa;
private LocalDateTime horarioDeApertura;
private LocalDateTime horarioDeCierre;
private int cantidadDeComensales;
private String nombreDelMozo;
private int cantidadDePlatos;
private int cantidadDeBebidas;
private  double precioDelPlato;
private double precioDeBebida;
private int metodoDePago;

//Método constructor
public Orden( int numeroMesa, LocalDateTime horarioDeApertura, LocalDateTime horarioDeCierre,
        int cantidadDeComensales, String nombreDelMozo,int cantidadDePlatos,int cantidadDeBebidas,double precioDelPlato,double precioDeBebida,int metodoDePago) {
   
    this.numeroMesa = numeroMesa;
    this.horarioDeApertura= horarioDeApertura;
    this.horarioDeCierre = horarioDeCierre;
    this.cantidadDeComensales = cantidadDeComensales;
    this.nombreDelMozo = nombreDelMozo;
    this.cantidadDePlatos=cantidadDePlatos;
    this.cantidadDeBebidas=cantidadDeBebidas;
     this.precioDelPlato=precioDelPlato; /// dar valor en test
     this.precioDeBebida=precioDeBebida; 
    this.metodoDePago=metodoDePago;
}

//Métodos setters y getters
public void setNumeroMesa(int mesa) {
    this.numeroMesa = mesa;
}

public int getNumeroMesa() {
    return numeroMesa;
}

public void setHorarioDeAperturaInicio(LocalDateTime horaDeInicio) {
    this.horarioDeApertura = horaDeInicio;
}

public LocalDateTime getHorararioDeApertura() {
    return horarioDeApertura;
}


public void setHoraCierre(LocalDateTime horaDeCierre) {
    this.horarioDeCierre = horaDeCierre;
}


public LocalDateTime getHorarioDeCierre() {
    return horarioDeCierre;
}


public void setCantidadDeComensales(int cantidadComensales) {
    this.cantidadDeComensales = cantidadComensales;
}

public int getCantidadDeComensales() {
    return cantidadDeComensales;
}


public void setNombreDelMozo(String nombreMozo) {
    this.nombreDelMozo = nombreMozo;
}

public String getNombreDelMozo() {
    return nombreDelMozo;
}


public void setCantidadPlatos(int cantidadPlatos) {
    this.cantidadDePlatos = cantidadPlatos;
}

public int getCantidadPlatos() {
    return cantidadDePlatos;
}


public void setCantidadBebidas(int cantidadBebidas) {
	this.cantidadDeBebidas = cantidadBebidas;
}

public int getCantidadBebidas() {
    return cantidadDeBebidas;
}


public void setPrecioPlato(double precioPlato) {
    this.precioDelPlato = precioPlato;
}

public double getPrecioPlato() {
    return precioDelPlato;
}


public void setPrecioBebida(double precioBebida) {
    this.precioDeBebida = precioBebida;
}

public double getPrecioBebida() {
    return precioDeBebida;
}


public void setMetodoDePago(int metodoPago) {
    this.metodoDePago = metodoPago;
}

public int getMetodoPago() {
    return metodoDePago;
}

}
